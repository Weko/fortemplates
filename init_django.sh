wget https://gitlab.com/Weko/fortemplates/-/raw/master/flake8.cfg
wget https://gitlab.com/Weko/fortemplates/-/raw/master/.pre-commit-config.yaml
wget https://gitlab.com/Weko/fortemplates/-/raw/master/.gitignore
wget https://gitlab.com/Weko/fortemplates/-/raw/master/requirements.txt
virtualenv ~/.env/$(basename "$PWD") --python=$(which python3.7)
. ~/.env/"$(basename $PWD)"/bin/activate
pip install -r requirements.txt
pre-commit install 
django-admin startproject config . 
mkdir config/settings
mv config/settings.py config/settings/common.py
wget -O config/settings/ https://gitlab.com/Weko/fortemplates/-/raw/master/common.py
wget -P config/settings/ https://gitlab.com/Weko/fortemplates/-/raw/master/local.dist.py
wget -P config/settings/ https://gitlab.com/Weko/fortemplates/-/raw/master/__init__.py
cat config/settings/local.dist.py > config/settings/local.py
mkdir app
touch app/.gitkeep
